from fastapi import FastAPI ,Response ,HTTPException ,status ,File, UploadFile
from fastapi.responses import JSONResponse



from fastapi.params import Body
from multipart import MultipartParser
import tensorflow as tf
import numpy as np
from PIL import Image
import io
import os


from pydantic import BaseModel
from random import randrange
import os



import tensorflow as tf
import numpy as np
from tensorflow.keras.preprocessing import image


app = FastAPI()

def getName():
    list=["Apple Black rot",
"Apple Cedar apple rust",
"Apple healthy",
"Bacterial leaf blight in rice leaf",
"Blight in corn Leaf",
"Blueberry healthy",
"Brown spot in rice leaf",
"Cercospora leaf spot",
"Cherry (including sour) Powdery mildew",
"Cherry (including_sour) healthy",
"Common Rust in corn Leaf",
"Corn (maize) healthy",
"Garlic",
"Grape Black rot",
"Grape Esca Black Measles",
"Grape Leaf blight Isariopsis Leaf Spot",
"Grape healthy",
"Gray Leaf Spot in corn Leaf",
"Leaf smut in rice leaf",
"Orange Haunglongbing Citrus greening",
"Peach healthy",
"Pepper bell Bacterial spot",
"Pepper bell healthy",
"Potato Early blight",
"Potato Late blight",
"Potato healthy",
"Raspberry healthy",
"Sogatella rice",
"Soybean healthy",
"Strawberry Leaf scorch",
"Strawberry healthy",
"Tomato Bacterial spot",
"Tomato Early blight",
"Tomato Late blight",
"Tomato Leaf Mold",
"Tomato Septoria leaf spot",
"Tomato Spider mites Two spotted spider mite",
"Tomato Target Spot",
"Tomato Tomato mosaic virus",
"Tomato healthy",
"algal leaf in tea",
"anthracnose in tea",
"bird eye spot in tea",
"brown blight in tea",
"cabbage looper",
"corn crop",
"ginger",
"healthy tea leaf",
"lemon canker",
"onion",
"potassium deficiency in plant",
"potato crop",
"potato hollow heart",
"red leaf spot in tea",
"tomato canker",
]

    return list

file_location="image.jpg"


model = tf.keras.models.load_model("model55.h5")


def preprocess_image(image):
    # Resize the image to 224x224 pixels
    image = image.resize((200, 200))
    # Convert the PIL image to a NumPy array
    img_array = np.array(image)
    # Reshape the array to have a batch dimension of 1
    img_array = np.expand_dims(img_array, axis=0)
    # Normalize the pixel values to be between 0 and 1
    # img_array = img_array / 255.0
    return img_array


@app.post("/uf")
async def create_upload_files(file: bytes = File(...)):

    # contents =   await file.read()

    image = Image.open(io.BytesIO(file))
    image =preprocess_image(image)

    predictions = model.predict(image)

    predicted_class = np.argmax(predictions[0])
    confidence = np.max(predictions[0])
    # print(f"Predicted class: {predicted_class}, confidence: {confidence}")
    my_dict = {"Totle class":  str(len(predictions[0])),"Predicted class": str(predicted_class),"Predicted class name": str(getName()[predicted_class]), "confidence": str(confidence), "Prediction": predictions.tolist()[0]}
   
    return JSONResponse(content=my_dict)



# @app.post("/uf")
# async def create_upload_files(file: UploadFile=File(...)):

#     image1 =   await file.read()
#     with open(file_location, "wb+") as file_object:
#         file_object.write(image1)
#     img = image.load_img(file_location, target_size=(200, 200))
#     img_array = image.img_to_array(img)

# # Expand the dimensions of the array to make its shape (1, 64, 64, 3)
#     img_array = tf.expand_dims(img_array, axis=0)
#     img_array /= 25.5
#     os.remove(file_location)

 
#     predictions = model.predict(img_array)

#     # predictions[0] = predictions[0] * 10000
#     # predictions[0]=np.ceil(predictions[0])

#     predicted_class = np.argmax(predictions[0])
#     confidence = np.max(predictions[0])
#     # print(f"Predicted class: {predicted_class}, confidence: {confidence}")
#     my_dict = {"Totle class":  str(len(predictions[0])),"Predicted class": str(predicted_class),"Predicted class name": str(getName()[predicted_class]), "confidence": str(confidence), "Prediction": predictions.tolist()[0]}
   
#     return  JSONResponse(content=my_dict)





   



















#my_dic=[]
    # class Post(BaseModel):
#     title : str
#     content: str
#     published : bool=True
#   #  rating : Optional[int] not working

# def find_post(id):
  
#     for i in my_dic:
#         if i['Id']==id:
#             return i


# @app.get("/")
# async def root():
#     return {"message": "Hello World"}

# @app.post("/post")
# async def upload_post(new_post: Post):
#     temp_dic=new_post.dict()
#     temp_dic["Id"]=randrange(0,1000000)
#     my_dic.append(temp_dic)
#     return {"data":temp_dic}

# @app.get("/posts")
# async def get_posts():
#     return {"data":my_dic}

# @app.get("/post/{id}")
# async def get_post(id ):
#     post= find_post(int(id))
#     if not post:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,detail=f"post with id {id} not found")
#     return {"Post":post}
